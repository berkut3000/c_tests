//#include <stdio.h>
#include <ncurses.h>
#include <stdint.h>
#include <stdbool.h>

/* count characters in input; 1st version */
uint8_t main()
{
	
	double nc = 0, nl = 0, ne = 0, nt = 0;
//	uint8_t bFlag = 0;
	int c = 0;
//	while ((c = getchar()) != EOF) {
//		putchar(c);
//
//		++nc;
//	}

	initscr();
	noecho();
	keypad(stdscr, TRUE);
	printw("Hello World!!!");
	refresh();
	
	for (nc = 0; (c = getch()) != KEY_LEFT; ++nc){
		if( c == KEY_BACKSPACE)
		{
			++nl;
//			bFlag = 0;
			addch('\\');
			c = 'b';
		}else if(c == '\\'){
			++ne;
//			bFlag += 1;
			addch('\\');
			c = '\\';
		}else if(c == '\t'){
			++nt;
//			bFlag = 0;
			addch('\\');
			c = 't';

		}

//		if(bFlag > 1 && c == ' ')
//		{
//			;
//		}else{
		addch(c);
//		}
	}
	endwin();
	printf("Numero de Caracteres = %.0f\r\n", nc);
	printf("Numero de Backspaces = %.0f\r\n", nl);
	printf("Numero de Slashes = %.0f\r\n", ne);
	printf("Numero de Tabuladores = %.0f\r\n", nt);
	return 0;
}
