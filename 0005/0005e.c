//#include <stdio.h>
#include <ncurses.h>
#include <stdint.h>
#include <stdbool.h>

#define IN  1/* inside word */
#define OUT 0 /* outside word */

/* count lines, words, and characters in input */
uint8_t main()
{
	
	double nc = 0, nl = 0, nw = 0; /* contadores de caracteres, lineas y palabras */
	int  bEstado = OUT;

	int c = 0;

	initscr();
	noecho();
	keypad(stdscr, TRUE);
	printw("Contador de palabras, lineas y caracteres");
	addch('\n');
	refresh();
	
	for (nc = 0; (c = getch()) != KEY_DC; ++nc){
		if( c == '\n')
		{
			++nl;
		}

		if( c == ' ' || c == '\n' || c == '\t')
		{
			bEstado = OUT;
		}else if(bEstado == OUT){
			bEstado = IN;
			++nw;
		}
		addch(c);
	}
	endwin();
	printf("Numero de Lineas = %.0f\r\n", nl);
	printf("Numero de Palabras = %.0f\r\n", nw);
	printf("Numero de Caracteres = %.0f\r\n", nc);
	return 0;
}
