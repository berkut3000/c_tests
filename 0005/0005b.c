#include <stdio.h>
#include <stdint.h>

/* count characters in input; 1st version */
uint8_t main()
{
	
	long nc = 0;

	int c;
	while ((c = getchar()) != EOF) {
//		putchar(c);

		++nc;
	}

	printf("%ld\r\n", nc);
	return 0;
}
