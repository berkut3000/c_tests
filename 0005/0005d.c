#include <stdio.h>
#include <stdint.h>

/* count characters in input; 1st version */
uint8_t main()
{
	
	double nc = 0, nl = 0;

	int c;
//	while ((c = getchar()) != EOF) {
//		putchar(c);
//
//		++nc;
//	}

	for (nc = 0; (c = getchar ()) != EOF; ++nc)
		if(c == '\n'){
			++nl;
		}

	printf("Numero de Caracteres = %.0f\r\n", nc);
	printf("Numero de NL = %.0f\r\n", nl);
	return 0;
}
