#include <stdio.h>
#include <stdint.h>

/* count characters in input; 1st version */
uint8_t main()
{
	
	double nc = 0, nl = 0, ne = 0, nt = 0;

	int c;
//	while ((c = getchar()) != EOF) {
//		putchar(c);
//
//		++nc;
//	}

	for (nc = 0; (c = getchar ()) != EOF; ++nc){
		if(c == '\n'){
			++nl;
		}
		if(c == ' '){
			++ne;
		}
		if(c == '\t'){
			++nt;
		}
	}

	printf("Numero de Caracteres = %.0f\r\n", nc);
	printf("Numero de NL = %.0f\r\n", nl);
	printf("Numero de Espacios = %.0f\r\n", ne);
	printf("Numero de Tabuladores = %.0f\r\n", nt);
	return 0;
}
