/*******************************************************************************
 * XXXX.h
 *
 *  Created on: DD/MM/2019
 *  Modified on: DD/MM/2019
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __HEADERFILE_H__
#define __HEADERFILE_H__

/* Libraries to be included */
#include "masterSetup.h"

/* Function prototypes of the source file this header file references to */
void adc_conf();
int16_t adc_read();




#endif /* __HEADERFILE_H__ */
/*** end of file ***/
