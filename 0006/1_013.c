//#include <stdio.h>
#include <ncurses.h>
#include <stdint.h>
#include <stdbool.h>

#define IN  1/* inside word */
#define OUT 0 /* outside word */

/* count lines, words, and characters in input */
uint8_t main()
{
	
	double nc = 0, nw = 0; /* contadores de caracteres, longitudes de palabras y palabras */
	bool  bEstado = OUT;

	int c = 0,i = 0, nl = 1;
	int nDigit[20];
	for (i = 0; i <20; ++i)
	{	
		nDigit[i] = 0;
	} 

	initscr();
	noecho();
	keypad(stdscr, TRUE);
	printw("Contador de palabras, longitudes de palabras y caracteres");
	addch('\n');
	refresh();
	
	for (nc = 0; (c = getch()) != KEY_DC; ++nc){
//		if( c == '\n')
//		{
//			++nl;
//		}

		if( c == ' ' || c == '\n' || c == '\t')
		{
			bEstado = OUT;
			++nDigit[nl-1];
			nl = 0; 
		}else if(bEstado == OUT){
			addch('\n');
			bEstado = IN;
			++nw;
		}
		addch(c);
		nl++;
	}
	endwin();
	printf("Ocurrencias = ");
	for (i = 0; i<20; ++i)
	{
		printf(" %d", nDigit[i]);
	}
	printf("\r\nNumero de Palabras = %.0f\r\n", nw);
	printf("Numero de Caracteres = %.0f\r\n", nc);
	return 0;
}
