# C Tests

This respository hosts some of my C tests and examples. Mainly for testing and demo purposes.

## Getting Started

In general, there are 2 ways to run the programas hereby contained:
 - With Code-Blocks, focused for a Windows-based enviroment.
 - With Bare bones GCC, for Linux/Mac-based enviroment.
 
The "Fixed_point_math" folder, contains a basic template with the bare minimum required to generate an executable for Linux.

### Prerequisites

For Windows, a full installation of Code::Blocks is recommended.

For Linux, it is required to have GCC and related dependent packages installed.

```
For Debian 9 Stretch, try the following command
    $  gcc -v
```

### Installing

For Windows, installation of CodeBlocks with MingWSys is recommended.
As of Monday 4th, April 2019, download

```
codeblocks-17.12mingw-setup.exe
```

from [here](http://www.codeblocks.org/downloads/26), run as administrator and follow the on-screen instructions.

For Linux, it should be installed with the system.

When copying the Linux template, ensure to edit

```
makefile
```

accordingly, so that every desired source is compiled, every object is linked, every required header is included and no *.o is left out during a cleanup.

## Running the tests

In Code::Blocks, simply run the Debug or Run buttons once the project has been loaded into the Workspace to test the programs.

In Linux, unless otherwise stated, try to run the

```
make
```

command to compile the sources; and

```
./name_of_output_program
```

to see its operation on a terminal or bash.


## Built With

* [GCC](https://www.gnu.org/software/gcc/) - The GNU Compiler Collection
* [Code::Blocks](http://www.codeblocks.org) - The open source, cross platform, free C, C++ and Fortran IDE

## Versioning

GitLab is used for Versioning.

## Authors

* **Antonio Aguilar Mota** - *Initial work* - [AntoMota](https://gitlab.com/berkut3000)

## License

This project is licensed under the...

## Acknowledgments

* [Joe Lemieux](https://www.embedded.com/discussion/other/4024639/Fixed-point-math-in-C)
* [Brian Joseph Spinos](https://stackoverflow.com/questions/5580761/why-use-double-pointer-or-why-use-pointers-to-pointers)
* [pmg](https://stackoverflow.com/questions/5580761/why-use-double-pointer-or-why-use-pointers-to-pointers)