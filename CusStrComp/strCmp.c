#include <stdio.h>
#include <stdint.h>

uint8_t my_strcmp(uint8_t *a, uint8_t *b);

uint8_t main()
{
	uint8_t cadenita1[] = "HolaPinche";
	uint8_t cadenita2[] = "Hola";
	
	int8_t valor_resultante;
	valor_resultante = my_strcmp(cadenita1,cadenita2);
	printf("Cadena 1 mayor que cadena 2: %i\r\n",valor_resultante);

	valor_resultante = my_strcmp(cadenita2,cadenita1);
        printf("Cadena 1 menor que cadena 2: %i\r\n",valor_resultante);

	valor_resultante = my_strcmp(cadenita2,cadenita2);
        printf("Cadena 1 igual que Cadena 2 %i",valor_resultante);

}

uint8_t my_strcmp(uint8_t *a, uint8_t *b)
{
	while (*a && *b && *a == *b)
	{
		++a;
		++b;
	}
	return (unsigned char)(*a) - (unsigned char)(*b);
}
