#include <stdio.h>
#include <stdint.h>
/* print Celsius-Fahrenheit table
for celsius = 0, 20, ..., 300 */

uint8_t main()
//main()
{
	float fahr, celsius;
	float lower, upper, step;

	lower = 0.0; /* lower limit of temperature scale */
	upper = 300.0; /* upper limit */
	step = 2.0; /* step size */

	celsius = lower;
	printf("Programa de conversion de temperatura\r\n\r\n");
	while (celsius <= upper) {
		fahr = (9.0 * (celsius) / 5.0) + 32;
		printf("%3.0f\t%6.3f\n", celsius, fahr);
		celsius += step;
	}
	return 0;
}
