#include <stdio.h>
#include <stdint.h>
/* print Fahrenheit-Celsius table
for fahr = 0, 20, ..., 300 */

uint8_t main()
//main()
{
	float fahr, celsius;
	float lower, upper, step;

	lower = 0.0; /* lower limit of temperature scale */
	upper = 300.0; /* upper limit */
	step = 20.0; /* step size */

	fahr = lower;
	printf("Programa de conversion de temperatura\r\n\r\n");
	while (fahr <= upper) {
		celsius = 5.0 * (fahr-32.0) / 9.0;
		printf("%3.0f\t%6.3f\n", fahr, celsius);
		fahr = fahr + step;
	}
	return 0;
}
