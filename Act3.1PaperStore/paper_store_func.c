/******************************************************************************
 * @ file paper_store_func.c
 *
 *  Created on: 16/02/2021
 *  Modified on: 16/02/2021
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "paper_store_func.h"

/* New data types */
struct cliente{
    int cliente_id;
    char cliente_nombre[100];
    struct cliente *next;
    struct cliente *prev;
};
typedef struct cliente cliente_t;

struct articulo{
    int articulo_id;
    char articulo_nombre[100];
    int articulo_precio;
    struct articulo *next;
    struct articulo *prev;
};
typedef struct articulo articulo_t;

struct servicio{
    int servicio_id;
    char servicio_nombre[100];
    int servicio_precio;
    struct servicio *next;
    struct servicio *prev;
};
typedef struct servicio servicio_t;

struct venta{
    int venta_id;
    char venta_nombre[100];
    int venta_articulos_total;
    int venta_servicios_total;
    int venta_precio;
    struct venta *next;
    struct venta *prev;
};
typedef struct venta venta_t;

/* Constant defintions */

/* Macro definitions */

/* Static Data */

/* Private function prototypes */
/* Clientes */
void clientes_list (cliente_t *head);
cliente_t *create_new_cliente(int value, char * name);
cliente_t *insert_cliente_at_head(cliente_t **head, cliente_t *node_to_insert);
cliente_t *find_cliente(cliente_t *head, int value);
cliente_t *find_last_cliente(cliente_t *head);
void insert_after_cliente(cliente_t *node_to_insert_after, cliente_t *newnode);
void remove_cliente(cliente_t **head, cliente_t * node_to_remove);

/* Articulos */
void articulos_list (articulo_t *head);
articulo_t *create_new_articulo(int value, char * name, int prize);
articulo_t *insert_articulo_at_head(articulo_t **head, articulo_t *node_to_insert);
articulo_t *find_articulo(articulo_t *head, int value);
articulo_t *find_last_articulo(articulo_t *head);
void insert_after_articulo(articulo_t *node_to_insert_after, articulo_t *newnode);
void remove_articulo(articulo_t **head, articulo_t * node_to_remove);

/* Servicios */
void servicios_list (servicio_t *head);
servicio_t *create_new_servicio(int value, char * name, int prize);
servicio_t *insert_servicio_at_head(servicio_t **head, servicio_t *node_to_insert);
servicio_t *find_servicio(servicio_t *head, int value);
servicio_t *find_last_servicio(servicio_t *head);
void insert_after_servicio(servicio_t *node_to_insert_after, servicio_t *newnode);
void remove_servicio(servicio_t **head, servicio_t * node_to_remove);

/* Ventas */
void ventas_list (venta_t *head);
venta_t *create_new_venta(int value, char * name, int art, int ser);
venta_t *insert_venta_at_head(venta_t **head, venta_t *node_to_insert);
venta_t *find_venta(venta_t *head, int value);
venta_t *find_last_venta(venta_t *head);
void insert_after_venta(venta_t *node_to_insert_after, venta_t *newnode);
void remove_venta(venta_t **head, venta_t * node_to_remove);


/* Public Functions */
/***************************************************************************//**
 * @brief
 *   Inicializa clientes de ejemplo.
 *
 * @details
 *   Inicializa clientes de ejemplo.
 *
 * @note
 *   None.
 *
 * @param[in] none
 *
 * @return
 *   Nothing.
 ******************************************************************************/
void clientes_init()
{
    cliente_add("Hugo");
    cliente_add("Gabriel");
    cliente_add("Raul");
    cliente_add("Pablo");
    cliente_add("Carlos");
    cliente_add("Luque");
}
/***************************************************************************//**
 * @brief
 *   Imprime la lista de clientes.
 *
 * @details
 *   Imprime la lista de clientes
 *
 * @note
 *   Se usa para ver la lista de clietnes actuales.
 *
 * @param[in] *head
 *   Apuntador a la cabeza; structura que contiene al primer cliente.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void clientes_list (cliente_t *head)
{
    cliente_t *temporary = head;
    printf("Cliente_ID\tCliente_Nombre\n");
    while (temporary != NULL)
    {
        printf("%d\t%s\n", temporary->cliente_id, temporary->cliente_nombre);
        temporary = temporary->next;
    }
    printf("\n");
}

/***************************************************************************//**
 * @brief
 *   Agrega un nuevo cliente.
 *
 * @details
 *   Agrega un nuevo cliente al final de la fila.
 *
 * @note
 *   Agrega un nuevo cliente a la cabeza, si la cabeza regresa un apuntador nulo
 *   caso contrario, lo agrega al final de la lista.El ID del cliente se asgina
 *   incrementando en uno el ID del cliente anterior.
 *
 * @param[in] char * cliente_nombre
 *   Nombre del cliente que se va a agregar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void cliente_add(char * cliente_nombre)
{
    cliente_t * tmp = find_last_cliente(clientes_head);
    if( NULL == tmp)
    {
        insert_cliente_at_head(&clientes_head, create_new_cliente(0, cliente_nombre));
    }else{
        insert_after_cliente(tmp, create_new_cliente((tmp->cliente_id + 1), cliente_nombre));
    }
}
/***************************************************************************//**
 * @brief
 *   Borra el cliente de la lista.
 *
 * @details
 *   Borra el cliente con el ID especificado; de la lista.
 *
 * @note
 *   Si el ID no se encuentra en la lista, retorna un mensaje reportando esto mismo.
 *
 * @param[in] cliente_delete_id
 *   ID del cliente que se desea eliminar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void cliente_delete(int cliente_delete_id)
{
    cliente_t * tmp;
    tmp = find_cliente(clientes_head, cliente_delete_id);
    if(NULL != tmp)
    {
        remove_cliente(&clientes_head, tmp);
    }else{
        printf("No se encontro el cliente especificado");
    }
}
/***************************************************************************//**
 * @brief
 *   Retorna informacion del cliente especificado.
 *
 * @details
 *   Retorna informacion del cliente especificado y la muestra en pantalla.
 *
 * @note
 *   Si no se llega a encontrar un cliente con el ID especificado, este se reporta en la lista.
 *
 * @param[in] int cliente_search_id
 *   ID del cliente a consultar.
 *
 * @return
 *   None.
 ******************************************************************************/
void cliente_consult(int cliente_search_id)
{
    cliente_t *tmp;
    tmp = find_cliente(clientes_head, cliente_search_id);
    if(NULL != tmp)
    {
        printf("Cliente_ID: %d\tCliente_Nombre: %s\n\n", tmp->cliente_id, tmp->cliente_nombre);
    }else{
        printf("No se pudo encontrar el cliente especificado\n\n");
    }
}

/* Ventas */
/***************************************************************************//**
 * @brief
 *   Inicializa ventas de ejemplo.
 *
 * @details
 *   Inicializa ventas de ejemplo.
 *
 * @note
 *   Lista de ventas y precio.
 *
 * @param[in] Nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void ventas_init()
{
    venta_add("Venta_1", 2,3);
    venta_add("Venta_2", 3,4);
    venta_add("Venta_3", 5,6);
    venta_add("Venta_4", 6,7);
    venta_add("Venta_5", 7,8);
    venta_add("Venta_6", 8,9);
}

/***************************************************************************//**
 * @brief
 *   Imprime la lista de ventas.
 *
 * @details
 *   Imprime el ID, el nombre de la venta, y el monto de la misma en lista.
 *
 * @note
 *   Util para saber lo que hay actualmente en la lista de ventas. Tambien permite saber el total de las ventas.
 *
 * @param[in] venta_t * head
 *   Apuntador a la cabeza de la lista de ventas, el primer elemento.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void ventas_list (venta_t *head)
{
    venta_t *temporary = head;
    int total_ventas= 0;
    //clear_screen();
    printf("venta_ID\t\tventa_Nombre\t\tventa_total_articulos\tventa_total_servicios\tventa_total\n");
    while (temporary != NULL)
    {
        #if 0
        printf("%d\t\t\t%s", temporary->venta_id, temporary->venta_nombre);
        printf("%*s", 18, "$:");
        printf("%d\n", temporary->venta_precio);
        #endif // 0
        printf("%-25d%-25s%-25d%-25d%-25d\n", temporary->venta_id, temporary->venta_nombre, temporary->venta_articulos_total, temporary->venta_servicios_total, temporary->venta_precio);
        total_ventas += temporary->venta_precio;
        temporary = temporary->next;
    }
    printf("Total de Ventas : %d\n\n", total_ventas);
}

/***************************************************************************//**
 * @brief
 *   Agrega una venta.
 *
 * @details
 *   Agrega una venta a la lista de ventas.
 *
 * @note
 *   Agrega una venta al final de la lista. Si la lista est� vac�a, la agrega a la cabeza.
 *   El ID de la venta se agrega incrementando en uno el valor del ID de la venta anterior.
 *
 * @param[in] char * venta_nombre
 *   Nombre que tomara la venta en la lista.
 *
 * @param[in] char * venta_precio
 *   Nombre del costo de la venta.
 *
 *
 * @return
 *   Nada.
 ******************************************************************************/
void venta_add(char * venta_nombre, int art_total, int ser_total)
{
    venta_t * tmp = find_last_venta(ventas_head);
    if( NULL == tmp)
    {
        insert_venta_at_head(&ventas_head, create_new_venta(0, venta_nombre, art_total, ser_total));
    }else{
        insert_after_venta(tmp, create_new_venta((tmp->venta_id + 1), venta_nombre, art_total, ser_total));
    }
}
/***************************************************************************//**
 * @brief
 *   Elimina una venta.
 *
 * @details
 *   Elimina una venta, que corresponda al ID especificado, de la lista.
 *
 * @note
 *   Si no encuenta una venta con el ID especificado, se muestra un mensaje reportandolo.
 *
 * @param[in] venta_delete_id
 *   ID de la venta que se desee eliminar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void venta_delete(int venta_delete_id)
{
    venta_t * tmp;
    tmp = find_venta(ventas_head, venta_delete_id);
    if(NULL != tmp)
    {
        remove_venta(&ventas_head, tmp);
    }else{
        printf("No se encontro el venta especificado");
    }
}
/***************************************************************************//**
 * @brief
 *   MUestra la venta.
 *
 * @details
 *   Muestra la informacion de la venta con el ID especificado.
 *
 * @note
 *   Si el ID de la venta no se encuentra, se mostrar� un mensaje report�ndolo.
 *
 * @param[in] venta_search_id
 *   El ID de la venta que se desea consultar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void venta_consult(int venta_search_id)
{
    venta_t *tmp;
    tmp = find_venta(ventas_head, venta_search_id);
    if(NULL != tmp)
    {
        printf("venta_ID\t\tventa_Nombre\t\tventa_total_articulos\tventa_total_servicios\tventa_total\n");
        printf("%-25d%-25s%-25d%-25d%-25d\n", tmp->venta_id, tmp->venta_nombre, tmp->venta_articulos_total, tmp->venta_servicios_total, tmp->venta_precio);
    }else{
        printf("No se pudo encontrar el venta especificado\n\n");
    }
}

/* Articulo */
/***************************************************************************//**
 * @brief
 *   Inicializa articulos de ejemplo.
 *
 * @details
 *   Inicializa articulos de ejemplo, con nombre y precio.
 *
 * @note
 *   Lista de ventas y precio.
 *
 * @param[in] Nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void articulos_init()
{
    articulo_add("Lapiz", 2);
    articulo_add("Goma", 3);
    articulo_add("Sacapuntas", 4);
    articulo_add("Regla", 5);
    articulo_add("Boligrafo", 6);
    articulo_add("Escuadra", 7);
}

/***************************************************************************//**
 * @brief
 *   Imprime la lista de articulos.
 *
 * @details
 *   Imprime el ID, el nombre del articulo, y su costo en la lista.
 *
 * @note
 *   Util para saber el estado actual de la lista de articulos.
 *
 * @param[in] articulo_t * head
 *   Apuntador a la cabeza de la lista de articulos, el primer elemento.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void articulos_list (articulo_t *head)
{
    articulo_t *temporary = head;
    //clear_screen();
    printf("Articulo_ID\t\tArticulo_Nombre\t\tArticulo_Precio\n");
    while (temporary != NULL)
    {
        #if 0
        printf("%d\t\t\t%s", temporary->articulo_id, temporary->articulo_nombre);
        printf("%*s", 18, "$:");
        printf("%d\n", temporary->articulo_precio);
        #endif
        printf("%-25d%-25s%-25d\n", temporary->articulo_id, temporary->articulo_nombre, temporary->articulo_precio);
        temporary = temporary->next;
    }
    printf("\n");
}

/***************************************************************************//**
 * @brief
 *   Agrega un articulo.
 *
 * @details
 *   Agrega un aarticulo a la lista de articulos.
 *
 * @note
 *   Agrega un articulo al final de la lista. Si la lista esta vacia, la agrega a la cabeza.
 *   El ID del articulo se agrega incrementando en uno el valor del ID del articulo anterior.
 *
 * @param[in] char * articulo_nombre
 *   Nombre que tomara el articulo en la lista.
 *
 * @param[in] char * articulo_precio
 *   Nombre del precio del articulo.
 *
 *
 * @return
 *   Nada.
 ******************************************************************************/
void articulo_add(char * articulo_nombre, int articulo_precio)
{
    articulo_t * tmp = find_last_articulo(articulos_head);
    if( NULL == tmp)
    {
        insert_articulo_at_head(&articulos_head, create_new_articulo(0, articulo_nombre, articulo_precio));
    }else{
        insert_after_articulo(tmp, create_new_articulo((tmp->articulo_id + 1), articulo_nombre, articulo_precio));
    }
}
/***************************************************************************//**
 * @brief
 *   Elimina un articulo.
 *
 * @details
 *   Elimina una articulo, que corresponda al ID especificado, de la lista.
 *
 * @note
 *   Si no encuenta un articulo con el ID especificado, se muestra un mensaje reportandolo.
 *
 * @param[in] aticulo_delete_id
 *   ID del articulo que se desee eliminar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void articulo_delete(int articulo_delete_id)
{
    articulo_t * tmp;
    tmp = find_articulo(articulos_head, articulo_delete_id);
    if(NULL != tmp)
    {
        remove_articulo(&articulos_head, tmp);
    }else{
        printf("No se encontro el articulo especificado");
    }
}
/***************************************************************************//**
 * @brief
 *   MUestra el articulo.
 *
 * @details
 *   Muestra la informacion del articulo con el ID especificado.
 *
 * @note
 *   Si el ID de la articulo no se encuentra, se mostrara un mensaje reportandolo.
 *
 * @param[in] venta_search_id
 *   El ID del articulo que se desea consultar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void articulo_consult(int articulo_search_id)
{
    articulo_t *tmp;
    tmp = find_articulo(articulos_head, articulo_search_id);
    if(NULL != tmp)
    {
        printf("articulo_ID: %d\t articulo_Nombre: %s\n\n", tmp->articulo_id, tmp->articulo_nombre);
    }else{
        printf("No se pudo encontrar el articulo especificado\n\n");
    }
}

/* Servicio */
/***************************************************************************//**
 * @brief
 *   Inicializa servicios de ejemplo.
 *
 * @details
 *   Inicializa servicios de ejemplo, con nombre y precio.
 *
 * @note
 *   Lista de articulos y precio.
 *
 * @param[in] Nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void servicios_init()
{
    servicio_add("Impresion", 2);
    servicio_add("Engargolado", 3);
    servicio_add("Enmicado", 4);
    servicio_add("Copia", 5);
    servicio_add("CopiaColor", 6);
    servicio_add("Escaneo", 7);
}

/***************************************************************************//**
 * @brief
 *   Imprime la lista de servicios.
 *
 * @details
 *   Imprime el ID, el nombre del servicio, y su costo en la lista.
 *
 * @note
 *   Util para saber el estado actual de la lista de servicios.
 *
 * @param[in] servicio_t * head
 *   Apuntador a la cabeza de la lista de servicios, el primer elemento.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void servicios_list (servicio_t *head)
{
    servicio_t *temporary = head;
    //clear_screen();
    printf("servicio_ID\t\tservicio_Nombre\t\tservicio_Precio\n");
    while (temporary != NULL)
    {
        #if 0
        printf("%d\t\t\t%s", temporary->servicio_id, temporary->servicio_nombre);
        printf("%*s", 30, "$:");
        printf("%-12d\n", temporary->servicio_precio);
        #endif
        printf("%-25d%-25s%-25d\n", temporary->servicio_id, temporary->servicio_nombre, temporary->servicio_precio);

        temporary = temporary->next;
    }
    printf("\n");
}

/***************************************************************************//**
 * @brief
 *   Agrega un servicio.
 *
 * @details
 *   Agrega un servicio a la lista de servicios.
 *
 * @note
 *   Agrega un servicio al final de la lista. Si la lista esta vacia, la agrega a la cabeza.
 *   El ID del servicio se agrega incrementando en uno el valor del ID del servicio anterior.
 *
 * @param[in] char * servicio_nombre
 *   Nombre que tomara el servicio en la lista.
 *
 * @param[in] char * servicio_precio
 *   Nombre del precio del servicio.
 *
 *
 * @return
 *   Nada.
 ******************************************************************************/
void servicio_add(char * servicio_nombre, int servicio_precio)
{
    servicio_t * tmp = find_last_servicio(servicios_head);
    if( NULL == tmp)
    {
        insert_servicio_at_head(&servicios_head, create_new_servicio(0, servicio_nombre, servicio_precio));
    }else{
        insert_after_servicio(tmp, create_new_servicio((tmp->servicio_id + 1), servicio_nombre, servicio_precio));
    }
}
/***************************************************************************//**
 * @brief
 *   Elimina un servicio.
 *
 * @details
 *   Elimina una servicio, que corresponda al ID especificado, de la lista.
 *
 * @note
 *   Si no encuenta un servicio con el ID especificado, se muestra un mensaje reportandolo.
 *
 * @param[in] aticulo_delete_id
 *   ID del servicio que se desee eliminar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void servicio_delete(int servicio_delete_id)
{
    servicio_t * tmp;
    tmp = find_servicio(servicios_head, servicio_delete_id);
    if(NULL != tmp)
    {
        remove_servicio(&servicios_head, tmp);
    }else{
        printf("No se encontro el servicio especificado");
    }
}
/***************************************************************************//**
 * @brief
 *   MUestra el servicio.
 *
 * @details
 *   Muestra la informacion del servicio con el ID especificado.
 *
 * @note
 *   Si el ID de la servicio no se encuentra, se mostrara un mensaje reportandolo.
 *
 * @param[in] venta_search_id
 *   El ID del servicio que se desea consultar.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void servicio_consult(int servicio_search_id)
{
    servicio_t *tmp;
    tmp = find_servicio(servicios_head, servicio_search_id);
    if(NULL != tmp)
    {
        printf("servicio_ID: %d\t servicio_Nombre: %s\n\n", tmp->servicio_id, tmp->servicio_nombre);
    }else{
        printf("No se pudo encontrar el servicio especificado\n\n");
    }
}


/* Private Functions */
/***************************************************************************//**
 * @brief
 *   Crea un nuevo cliente.
 *
 * @details
 *   Crea un nuevo cliente con ID y nombre especificados.
 *
 * @note
 *   Función exclusiva para cliente.
 *
 * @param[in] int value.
 *      Id del cliente
 *
 * @param[in] char * name.
 *      Apuntador al strign que contiene Nombre del cliente
 *
 * @return
 *   Apuntador a el cliente resultante.
 ******************************************************************************/
cliente_t *create_new_cliente(int value, char * name)
{
    cliente_t *result = malloc(sizeof(cliente_t));
    result->cliente_id = value;
    strcpy(result->cliente_nombre, name);
    result->next = NULL;
    result->prev = NULL;
    return result;
}

/***************************************************************************//**
 * @brief
 *   Inserta un nuevo cliente al inicio de la lista.
 *
 * @details
 *   inserta un nuevo cliente en la cabeza de la lista.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] cliente_t **head.
 *      Apuntador al apuntador de la cabeza de la lista. Esto nos permite modificar
 *      la lista en sí misma.
 *
 * @param[in] cliente_t *node_to_insert.
 *      Apuntador al nuevo cliente que se insertará.
 *
 * @return
 *   Apuntador al nuevo cliente.
 ******************************************************************************/
cliente_t *insert_cliente_at_head(cliente_t **head, cliente_t *node_to_insert)
{
    node_to_insert->next = *head;
    if(*head != NULL)
    {
        (*head)->prev = node_to_insert;
    }
    *head = node_to_insert;
    node_to_insert->prev = NULL;
    return node_to_insert;

}
/***************************************************************************//**
 * @brief
 *   Encuentra un cliente de la lista.
 *
 * @details
 *   Encuentra un nuevo cliente en la lista, cuyo ID concuerde con el parametro value.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] cliente_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 * @param[in] int value.
 *      ID del cliente a buscar.
 *
 * @return
 *   Apuntador al nuevo cliente.
 ******************************************************************************/
cliente_t *find_cliente(cliente_t *head, int value)
{
    cliente_t *tmp = head;
    while(tmp != NULL)
    {
        if(value == tmp->cliente_id )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Encuentra al último cliente de la lista.
 *
 * @details
 *   Encuentra al último cliente de la lista.
 *
 * @note
 *   Funcion exclusiva para clientes.
 *
 * @param[in] cliente_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 *
 * @return
 *   Apuntador al cliente encontrado, de no encontrarlo, retorna NULL.
 ******************************************************************************/
cliente_t *find_last_cliente(cliente_t *head)
{
    cliente_t *tmp = head;
    while(tmp != NULL)
    {
        if(NULL == tmp->next )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Inserta un cliente; después de un cliente especificado.
 *
 * @details
*   Inserta un cliente; después de un cliente especificado.
 *
 * @note
 *   Debe haber un nodo en la lista para que esto funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias. De ser
 *   así, usar insert_cliente_at_head.
 *
 * @param[in] cliente_t *node_to_insert_after.
 *      Apuntador al cliente despues del cual se insertara el nuevo cliente.
 *
 * @param[in] cliente_t *newnode.
 *      Apuntador al nuevo cliente.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void insert_after_cliente(cliente_t *node_to_insert_after, cliente_t *newnode)
{
    newnode->next = node_to_insert_after->next;
    if(newnode->next != NULL)
    {
        newnode->next->prev = node_to_insert_after;
    }
    newnode->prev = node_to_insert_after;
    node_to_insert_after->next = newnode;

}
/***************************************************************************//**
 * @brief
 *   Elimina un cliente.
 *
 * @details
*   Elimna un cliente, de la lista de clientes.
 *
 * @note
 *   Debe haber un nodo en la lista para que eso funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias. De ser
 *   así, usar insert_cliente_at_head.
 *
 * @param[in] cliente_t **head.
 *      Apuntador al apuntdor de la lista de clientes, esto es importante por si se solo
 *      queda un nodo en la lista.
 *
 * @param[in] cliente_t * node_to_remove.
 *      Apuntador al cliente que se eliminará.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void remove_cliente(cliente_t **head, cliente_t * node_to_remove)
{
    if(*head == node_to_remove)
    {
        *head = node_to_remove->next;
        if (*head != NULL)
        {
            (*head)->prev = NULL;
        }
    }else{
        //find the previous node in the list
        node_to_remove->prev->next = node_to_remove->next;
        if(node_to_remove->next != NULL)
        {
            node_to_remove->next->prev = node_to_remove->prev;
        }
    }
    node_to_remove->next = NULL;
    node_to_remove->prev = NULL;
    return;
}


/* Articulo */
/***************************************************************************//**
 * @brief
 *   Crea un nuevo articulo.
 *
 * @details
 *   Crea un nuevo articulo con ID, nombre y precio especificados.
 *
 * @note
 *   Función exclusiva para articulo.
 *
 * @param[in] int value.
 *      Id del articulo
 *
 * @param[in] char * name.
 *      Apuntador al string que contiene Nombre del articulo
 *
 * @param[in] int prize.
 *      Precio del articulo
 *
 * @return
 *   Apuntador al articulo resultante.
 ******************************************************************************/
articulo_t *create_new_articulo(int value, char * name, int prize)
{
    articulo_t *result = malloc(sizeof(articulo_t));
    result->articulo_id = value;
    strcpy(result->articulo_nombre, name);
    result->articulo_precio = prize;
    result->next = NULL;
    result->prev = NULL;
    return result;
}
/***************************************************************************//**
 * @brief
 *   Inserta un nuevo articulo al inicio de la lista.
 *
 * @details
 *   inserta un nuevo articulo en la cabeza de la lista.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] articulo_t **head.
 *      Apuntador al apuntador de la cabeza de la lista. Esto nos permite modificar
 *      la lista en sí misma.
 *
 * @param[in] articulo_t *node_to_insert.
 *      Apuntador al nuevo articulo que se insertará.
 *
 * @return
 *   Apuntador al nuevo articulo.
 ******************************************************************************/
articulo_t *insert_articulo_at_head(articulo_t **head, articulo_t *node_to_insert)
{
    node_to_insert->next = *head;
    if(*head != NULL)
    {
        (*head)->prev = node_to_insert;
    }
    *head = node_to_insert;
    node_to_insert->prev = NULL;
    return node_to_insert;

}
/***************************************************************************//**
 * @brief
 *   Encuentra un articulo de la lista.
 *
 * @details
 *   Encuentra un nuevo articulo en la lista, cuyo ID concuerde con el parametro value.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] articulo_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 * @param[in] int value.
 *      ID del articulo a buscar.
 *
 * @return
 *   Apuntador al nuevo articulo.
 ******************************************************************************/
articulo_t *find_articulo(articulo_t *head, int value)
{
    articulo_t *tmp = head;
    while(tmp != NULL)
    {
        if(value == tmp->articulo_id )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Encuentra al último articulo de la lista.
 *
 * @details
 *   Encuentra al último articulo de la lista.
 *
 * @note
 *   Funcion exclusiva para articulos.
 *
 * @param[in] articulo_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 *
 * @return
 *   Apuntador al articulo encontrado, de no encontrarlo, retorna NULL.
 ******************************************************************************/
articulo_t *find_last_articulo(articulo_t *head)
{
    articulo_t *tmp = head;
    while(tmp != NULL)
    {
        if(NULL == tmp->next )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Inserta un articulo; después de un articulo especificado.
 *
 * @details
*   Inserta un articulo; después de un articulo especificado.
 *
 * @note
 *   Debe haber un nodo en la lista para que esto funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias. De ser
 *   así, usar insert_articulo_at_head.
 *
 * @param[in] articulo_t *node_to_insert_after.
 *      Apuntador al articulo despues del cual se insertara el nuevo articulo.
 *
 * @param[in] articulo_t *newnode.
 *      Apuntador al nuevo articulo.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void insert_after_articulo(articulo_t *node_to_insert_after, articulo_t *newnode)
{
    newnode->next = node_to_insert_after->next;
    if(newnode->next != NULL)
    {
        newnode->next->prev = node_to_insert_after;
    }
    newnode->prev = node_to_insert_after;
    node_to_insert_after->next = newnode;

}
/***************************************************************************//**
 * @brief
 *   Elimina un articulo.
 *
 * @details
*   Elimna un articulo, de la lista de articulos.
 *
 * @note
 *   Debe haber un nodo en la lista para que eso funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias. De ser
 *   así, usar insert_articulo_at_head.
 *
 * @param[in] articulo_t **head.
 *      Apuntador al apuntdor de la lista de articulos, esto es importante por si se solo
 *      queda un nodo en la lista.
 *
 * @param[in] articulo_t * node_to_remove.
 *      Apuntador al articulo que se eliminará.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void remove_articulo(articulo_t **head, articulo_t * node_to_remove)
{
    if(*head == node_to_remove)
    {
        *head = node_to_remove->next;
        if (*head != NULL)
        {
            (*head)->prev = NULL;
        }
    }else{
        //find the previous node in the list
        node_to_remove->prev->next = node_to_remove->next;
        if(node_to_remove->next != NULL)
        {
            node_to_remove->next->prev = node_to_remove->prev;
        }
    }
    node_to_remove->next = NULL;
    node_to_remove->prev = NULL;
    return;
}

/* Servicio */
/***************************************************************************//**
 * @brief
 *   Crea un nuevo servicio.
 *
 * @details
 *   Crea un nuevo servicio con ID, nombre y precio especificados.
 *
 * @note
 *   Función exclusiva para servicio.
 *
 * @param[in] int value.
 *      Id del servicio
 *
 * @param[in] char * name.
 *      Apuntador al string que contiene Nombre del servicio
 *
 * @param[in] int prize.
 *      Precio del servicio
 *
 * @return
 *   Apuntador al servicio resultante.
 ******************************************************************************/
servicio_t *create_new_servicio(int value, char * name, int prize)
{
    servicio_t *result = malloc(sizeof(servicio_t));
    result->servicio_id = value;
    strcpy(result->servicio_nombre, name);
    result->servicio_precio = prize;
    result->next = NULL;
    result->prev = NULL;
    return result;
}
/***************************************************************************//**
 * @brief
 *   Inserta un nuevo servicio al inicio de la lista.
 *
 * @details
 *   inserta un nuevo servicio en la cabeza de la lista.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] servicio_t **head.
 *      Apuntador al apuntador de la cabeza de la lista. Esto nos permite modificar
 *      la lista en sí misma.
 *
 * @param[in] servicio_t *node_to_insert.
 *      Apuntador al nuevo servicio que se insertará.
 *
 * @return
 *   Apuntador al nuevo servicio.
 ******************************************************************************/
servicio_t *insert_servicio_at_head(servicio_t **head, servicio_t *node_to_insert)
{
    node_to_insert->next = *head;
    if(*head != NULL)
    {
        (*head)->prev = node_to_insert;
    }
    *head = node_to_insert;
    node_to_insert->prev = NULL;
    return node_to_insert;

}
/***************************************************************************//**
 * @brief
 *   Encuentra un servicio de la lista.
 *
 * @details
 *   Encuentra un nuevo servicio en la lista, cuyo ID concuerde con el parametro value.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] servicio_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 * @param[in] int value.
 *      ID del servicio a buscar.
 *
 * @return
 *   Apuntador al nuevo servicio.
 ******************************************************************************/
servicio_t *find_servicio(servicio_t *head, int value)
{
    servicio_t *tmp = head;
    while(tmp != NULL)
    {
        if(value == tmp->servicio_id )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Encuentra al último servicio de la lista.
 *
 * @details
 *   Encuentra al último servicio de la lista.
 *
 * @note
 *   Funcion exclusiva para servicios.
 *
 * @param[in] servicio_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 *
 * @return
 *   Apuntador al servicio encontrado, de no encontrarlo, retorna NULL.
 ******************************************************************************/
servicio_t *find_last_servicio(servicio_t *head)
{
    servicio_t *tmp = head;
    while(tmp != NULL)
    {
        if(NULL == tmp->next )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Inserta un servicio; después de un servicio especificado.
 *
 * @details
*   Inserta un servicio; después de un servicio especificado.
 *
 * @note
 *   Debe haber un nodo en la lista para que esto funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias. De ser
 *   así, usar insert_servicio_at_head.
 *
 * @param[in] servicio_t *node_to_insert_after.
 *      Apuntador al servicio despues del cual se insertara el nuevo servicio.
 *
 * @param[in] servicio_t *newnode.
 *      Apuntador al nuevo servicio.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void insert_after_servicio(servicio_t *node_to_insert_after, servicio_t *newnode)
{
    newnode->next = node_to_insert_after->next;
    if(newnode->next != NULL)
    {
        newnode->next->prev = node_to_insert_after;
    }
    newnode->prev = node_to_insert_after;
    node_to_insert_after->next = newnode;

}
/***************************************************************************//**
 * @brief
 *   Elimina un servicio.
 *
 * @details
*   Elimna un servicio, de la lista de servicios.
 *
 * @note
 *   Debe haber un nodo en la lista para que eso funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias. De ser
 *   así, usar insert_servicio_at_head.
 *
 * @param[in] servicio_t **head.
 *      Apuntador al apuntdor de la lista de servicios, esto es importante por si se solo
 *      queda un nodo en la lista.
 *
 * @param[in] servicio_t * node_to_remove.
 *      Apuntador al servicio que se eliminará.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void remove_servicio(servicio_t **head, servicio_t * node_to_remove)
{
    if(*head == node_to_remove)
    {
        *head = node_to_remove->next;
        if (*head != NULL)
        {
            (*head)->prev = NULL;
        }
    }else{
        //find the previous node in the list
        node_to_remove->prev->next = node_to_remove->next;
        if(node_to_remove->next != NULL)
        {
            node_to_remove->next->prev = node_to_remove->prev;
        }
    }
    node_to_remove->next = NULL;
    node_to_remove->prev = NULL;
    return;
}

/* Ventas */
/***************************************************************************//**
 * @brief
 *   Crea una nueva venta.
 *
 * @details
 *   Crea una nueva venta con ID, nombre y precio especificados.
 *
 * @note
 *   Función exclusiva para venta.
 *
 * @param[in] int value.
 *      Id de la venta
 *
 * @param[in] char * name.
 *      Apuntador al string que contiene Nombre de la venta
 *
 * @param[in] int prize.
 *      Precio de la venta
 *
 * @return
 *   Apuntador a la venta resultante.
 ******************************************************************************/
venta_t *create_new_venta(int value, char * name, int art, int ser)
{
    venta_t *result = malloc(sizeof(venta_t));
    result->venta_id = value;
    strcpy(result->venta_nombre, name);
    result->venta_articulos_total = art;
    result->venta_servicios_total = ser;
    result->venta_precio = result->venta_articulos_total + result->venta_servicios_total;
    result->next = NULL;
    result->prev = NULL;
    return result;
}
/***************************************************************************//**
 * @brief
 *   Inserta una nueva venta al inicio de la lista.
 *
 * @details
 *   inserta una nueva venta en la cabeza de la lista.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] venta_t **head.
 *      Apuntador al apuntador de la cabeza de la lista. Esto nos permite modificar
 *      la lista en sí misma.
 *
 * @param[in] venta_t *node_to_insert.
 *      Apuntador a la nueva venta que se insertará.
 *
 * @return
 *   Apuntador a la nueva venta.
 ******************************************************************************/
venta_t *insert_venta_at_head(venta_t **head, venta_t *node_to_insert)
{
    node_to_insert->next = *head;
    if(*head != NULL)
    {
        (*head)->prev = node_to_insert;
    }
    *head = node_to_insert;
    node_to_insert->prev = NULL;
    return node_to_insert;

}
/***************************************************************************//**
 * @brief
 *   Encuentra una venta de la lista.
 *
 * @details
 *   Encuentra una nueva venta en la lista, cuyo ID concuerde con el parametro value.
 *
 * @note
 *   Importante para inicializar cualquier lista.
 *
 * @param[in] venta_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 * @param[in] int value.
 *      ID de la venta a buscar.
 *
 * @return
 *   Apuntador a la nueva venta.
 ******************************************************************************/
venta_t *find_venta(venta_t *head, int value)
{
    venta_t *tmp = head;
    while(tmp != NULL)
    {
        if(value == tmp->venta_id )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Encuentra a la ultima venta de la lista.
 *
 * @details
 *   Encuentra a la ultima venta de la lista.
 *
 * @note
 *   Funcion exclusiva para ventas.
 *
 * @param[in] venta_t *head.
 *      Apuntador de la cabeza de la lista.
 *
 *
 * @return
 *   Apuntador al venta encontrado, de no encontrarlo, retorna NULL.
 ******************************************************************************/
venta_t *find_last_venta(venta_t *head)
{
    venta_t *tmp = head;
    while(tmp != NULL)
    {
        if(NULL == tmp->next )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/***************************************************************************//**
 * @brief
 *   Inserta un venta; después de un venta especificado.
 *
 * @details
*   Inserta un venta; después de un venta especificado.
 *
 * @note
 *   Debe haber un nodo en la lista para que esto funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias. De ser
 *   así, usar insert_venta_at_head.
 *
 * @param[in] venta_t *node_to_insert_after.
 *      Apuntador a la venta despues del cual se insertara la nueva venta.
 *
 * @param[in] venta_t *newnode.
 *      Apuntador a la nueva venta.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void insert_after_venta(venta_t *node_to_insert_after, venta_t *newnode)
{
    newnode->next = node_to_insert_after->next;
    if(newnode->next != NULL)
    {
        newnode->next->prev = node_to_insert_after;
    }
    newnode->prev = node_to_insert_after;
    node_to_insert_after->next = newnode;

}
/***************************************************************************//**
 * @brief
 *   Elimina una venta.
 *
 * @details
*   Elimna una venta, de la lista de ventas.
 *
 * @note
 *   Debe haber un nodo en la lista para que eso funcion. Importante encapsular
 *   esta funcionalidad para comprobar que no se escriba en lista vacias.
 *
 * @param[in] venta_t **head.
 *      Apuntador al apuntdor de la lista de ventas, esto es importante por si solo
 *      queda un nodo en la lista.
 *
 * @param[in] venta_t * node_to_remove.
 *      Apuntador al articulo que se eliminará.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void remove_venta(venta_t **head, venta_t * node_to_remove)
{
    if(*head == node_to_remove)
    {
        *head = node_to_remove->next;
        if (*head != NULL)
        {
            (*head)->prev = NULL;
        }
    }else{
        //find the previous node in the list
        node_to_remove->prev->next = node_to_remove->next;
        if(node_to_remove->next != NULL)
        {
            node_to_remove->next->prev = node_to_remove->prev;
        }
    }
    node_to_remove->next = NULL;
    node_to_remove->prev = NULL;
    return;
}
/*** end of file ***/
