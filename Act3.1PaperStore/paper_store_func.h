/*******************************************************************************
 * paper_store_func.h
 *
 *  Created on: 16/02/2021
 *  Modified on: 16/02/2021
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __PAPER_STORE_FUNC_H__
#define __PAPER_STORE_FUNC_H__

/* Libraries to be included */
typedef struct cliente cliente_t;
cliente_t *clientes_head;
cliente_t *clientes_tmp;

typedef struct articulo articulo_t;
articulo_t *articulos_head;
articulo_t *articulos_tmp;

typedef struct servicio servicio_t;
servicio_t *servicios_head;
servicio_t *servicios_tmp;

typedef struct venta venta_t;
venta_t *ventas_head;
venta_t *ventas_tmp;

/* Function prototypes of the source file this header file references to */
/* Clientes */
void clientes_init();
void clientes_list (cliente_t *head);
void cliente_add( char * cliente_nombre);
void cliente_delete(int cliente_delete_id);
void cliente_consult(int cliente_search_id);

/* Ventas */
void ventas_init();
void ventas_list (venta_t *head);
void venta_add( char * venta_nombre, int art_total, int ser_total);
void venta_delete(int venta_delete_id);
void venta_consult(int venta_search_id);

/* Articulos*/
void articulos_init();
void articulos_list (articulo_t *head);
void articulo_add( char * articulo_nombre, int articulo_precio);
void articulo_delete(int articulo_delete_id);
void articulo_consult(int articulo_search_id);

/* Servicios */
void servicios_init();
void servicios_list (servicio_t *head);
void servicio_add( char * servicio_nombre, int servicio_precio);
void servicio_delete(int servicio_delete_id);
void servicio_consult(int servicio_search_id);

#endif /* __HEADERFILE_H__ */
/*** end of file ***/
