#include <stdio.h>
#include <stdlib.h>
#include "paper_store_func.h"
#include <stdbool.h>

#define TRUE 1
#define FALSE 0
#define CLIENTES 1
#define ARTICULOS 2
#define SERVICIOS 3
#define VENTAS 4
#define SALIR 7

#define ALTAS 1
#define BAJAS 2
#define CONSULTAS 3

#if 0
struct venta{
    struct cliente;
    int total;
};

struct articulo{
    char articulo_nombre[100];
    int precio;

};
#endif
void clear_screen()
{
    for(int i = 0; i<=50; i++)
    {
        printf("\n");
    }
}
/***************************************************************************//**
 * @brief
 *   Menu de Clientes.
 *
 * @details
 *   Permite agregar, eliminar o consultar clientes.
 *
 * @note
 *   Regresa al menu anterior con 7.
 *
 * @param[in] nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void clientes_menu()
{
    bool exec_flag = TRUE;
    int option = 0;
    int cliente_search_query = 0;
    int cliente_delete_id = 0;
    char cliente_new_nombre[100] = "Oscar";

    while(exec_flag)
    {
        clientes_list(clientes_head);
        printf("Selecciona una opcion para clientes:\n");
        printf("\t1.Altas\n");
        printf("\t2.Bajas\n");
        printf("\t3.Consultas\n");
        printf("\t7.Regresar al menu anterior\n");
        scanf("%d", &option);
        clear_screen();
        switch(option)
        {
            case ALTAS:
                printf("Seleccionaste Altas\n");
                printf("Ingrese el nombre del nuevo cliente\n");
                scanf("%s", &cliente_new_nombre);
                cliente_add(cliente_new_nombre);
                break;
            case BAJAS:
                printf("Seleccionaste Bajas\n");
                printf("Ingrese el ID del cliente a eliminar\n");
                scanf("%d", &cliente_delete_id);
                cliente_delete(cliente_delete_id);
                break;
            case CONSULTAS:
                printf("Seleccionaste Consultas\n");
                printf("Ingrese el ID de cliente que desea buscar\n");
                scanf("%d", &cliente_search_query);
                cliente_consult(cliente_search_query);
                break;
            case SALIR:
                exec_flag = FALSE;
                break;
            default:
                clear_screen();
                break;
        }
    }
}
/***************************************************************************//**
 * @brief
 *   Menu de Articulos.
 *
 * @details
 *   Permite agregar, eliminar o consultar articulos.
 *
 * @note
 *   Regresa al menu anterior con 7.
 *
 * @param[in] nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void articulos_menu()
{
    bool exec_flag = TRUE;
    int option = 0;
    int articulo_search_query = 0;
    int articulo_delete_id = 0;
    int articulo_new_precio = 0;
    char articulo_new_nombre[100] = "Oscar";

    while(exec_flag)
    {
        articulos_list(articulos_head);
        printf("Selecciona una opcion para articulos:\n");
        printf("\t1.Altas\n");
        printf("\t2.Bajas\n");
        printf("\t3.Consultas\n");
        printf("\t7.Regresar al menu anterior\n");
        scanf("%d", &option);
        clear_screen();
        switch(option)
        {
            case ALTAS:
                printf("Seleccionaste Altas\n");
                printf("Ingrese el nombre del nuevo articulo\n");
                scanf("%s", &articulo_new_nombre);
                printf("Ingrese el precio del nuevo articulo\n");
                scanf("%d", &articulo_new_precio);
                articulo_add(articulo_new_nombre, articulo_new_precio);
                break;
            case BAJAS:
                printf("Seleccionaste Bajas\n");
                printf("Ingrese el ID del articulo a eliminar\n");
                scanf("%d", &articulo_delete_id);
                articulo_delete(articulo_delete_id);
                break;
            case CONSULTAS:
                printf("Seleccionaste Consultas\n");
                printf("Ingrese el ID de articulo que desea buscar\n");
                scanf("%d", &articulo_search_query);
                articulo_consult(articulo_search_query);
                break;
            case SALIR:
                exec_flag = FALSE;
                break;
            default:
                clear_screen();
                break;
        }
    }
}
/***************************************************************************//**
 * @brief
 *   Menu de Servicios.
 *
 * @details
 *   Permite agregar, eliminar o consultar servicios.
 *
 * @note
 *   Regresa al menu anterior con 7.
 *
 * @param[in] nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void servicios_menu()
{
    bool exec_flag = TRUE;
    int option = 0;
    int servicio_search_query = 0;
    int servicio_delete_id = 0;
    int servicio_new_precio = 0;
    char servicio_new_nombre[100] = "Oscar";

    while(exec_flag)
    {
        servicios_list(servicios_head);
        printf("Selecciona una opcion para servicios:\n");
        printf("\t1.Altas\n");
        printf("\t2.Bajas\n");
        printf("\t3.Consultas\n");
        printf("\t7.Regresar al menu anterior\n");
        scanf("%d", &option);
        clear_screen();
        switch(option)
        {
            case ALTAS:
                printf("Seleccionaste Altas\n");
                printf("Ingrese el nombre del nuevo servicio\n");
                scanf("%s", &servicio_new_nombre);
                printf("Ingrese el precio del nuevo servicio\n");
                scanf("%d", &servicio_new_precio);
                servicio_add(servicio_new_nombre, servicio_new_precio);
                break;
            case BAJAS:
                printf("Seleccionaste Bajas\n");
                printf("Ingrese el ID del servicio a eliminar\n");
                scanf("%d", &servicio_delete_id);
                servicio_delete(servicio_delete_id);
                break;
            case CONSULTAS:
                printf("Seleccionaste Consultas\n");
                printf("Ingrese el ID de servicio que desea buscar\n");
                scanf("%d", &servicio_search_query);
                servicio_consult(servicio_search_query);
                break;
            case SALIR:
                exec_flag = FALSE;
                break;
            default:
                clear_screen();
                break;
        }
    }
}
/***************************************************************************//**
 * @brief
 *   Menu de Ventas.
 *
 * @details
 *   Permite agregar, eliminar o consultar ventas.
 *
 * @note
 *   Regresa al menu anterior con 7.
 *
 * @param[in] nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
void ventas_menu()
{
    bool exec_flag = TRUE;
    int option = 0;
    int venta_search_query = 0;
    int venta_delete_id = 0;
    int venta_new_articulos_total = 0;
    int venta_new_servicios_total = 0;
    char venta_new_nombre[100] = "Oscar";

    while(exec_flag)
    {
        ventas_list(ventas_head);
        printf("Selecciona una opcion para ventas:\n");
        printf("\t1.Altas\n");
        printf("\t2.Bajas\n");
        printf("\t3.Consultas\n");
        printf("\t7.Regresar al menu anterior\n");
        scanf("%d", &option);
        clear_screen();
        switch(option)
        {
            case ALTAS:
                printf("Seleccionaste Altas\n");
                printf("Ingrese el nombre del nuevo venta\n");
                scanf("%s", &venta_new_nombre);
                printf("Ingrese el total de articulos\n");
                scanf("%d", &venta_new_articulos_total);
                printf("Ingrese el total de servicios\n");
                scanf("%d", &venta_new_servicios_total);
                venta_add(venta_new_nombre, venta_new_articulos_total, venta_new_servicios_total);
                break;
            case BAJAS:
                printf("Seleccionaste Bajas\n");
                printf("Ingrese el ID del venta a eliminar\n");
                scanf("%d", &venta_delete_id);
                venta_delete(venta_delete_id);
                break;
            case CONSULTAS:
                printf("Seleccionaste Consultas\n");
                printf("Ingrese el ID de venta que desea buscar\n");
                scanf("%d", &venta_search_query);
                venta_consult(venta_search_query);
                break;
            case SALIR:
                exec_flag = FALSE;
                break;
            default:
                clear_screen();
                break;
        }
    }
}
/***************************************************************************//**
 * @brief
 *   Main.
 *
 * @details
 *   Inicializa listas y menu principal.
 *
 * @note
 *   Salir del programa con 7.
 *
 * @param[in] nada.
 *
 * @return
 *   Nada.
 ******************************************************************************/
int main()
{
    bool exec_flag = TRUE;
    int option = 0;
    clientes_head = NULL;
    articulos_head = NULL;
    servicios_head = NULL;
    ventas_head = NULL;


    clientes_init();
    articulos_init();
    servicios_init();
    ventas_init();

    while(exec_flag)
    {
        printf("Selecciona una opcion:\n");
        printf("\t1.Clientes\n");
        printf("\t2.Articulos\n");
        printf("\t3.Servicios\n");
        printf("\t4.Ventas\n");
        printf("\t7.Salir\n");
        scanf("%d", &option);

        switch(option)
        {
            case CLIENTES:
                clientes_menu();
                break;
            case ARTICULOS:
                articulos_menu();
                break;
            case SERVICIOS:
                servicios_menu();
                break;
            case VENTAS:
                ventas_menu();
                break;
            case SALIR:
                exec_flag = FALSE;
                clear_screen();
                printf("Hasta Luego, Vuelva Pronto");
                break;
            default:
                clear_screen();
                break;

        }
    }
}
