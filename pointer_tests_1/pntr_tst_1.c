#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


void cant_change(int * x, int * z);
void change(int ** x, int * z);

int main(){

    int a = 1;
    int b = 2;
    int c = 3;
    int * p_a = &a;
    int * p_b = &b;
    int * p_c = &c;

    int ** pp_a = &p_a;  // pointer to pointer 'a'

    printf("\n a's value: %x \n", *p_a);
    printf("\n p_a's value: %x \n", p_a);
    printf("\n b's value: %x \n", *p_b);
    printf("\n p_b's value: %x \n", p_b);
    printf("\n c's value: %x \n", *p_c);
    printf("\n p_c's value: %x \n", p_c);
    printf("\n pp_a's value is %x \n", pp_a);
    printf("\n pp_a's stored value is %x \n", *pp_a);
    printf("\n pp_a's stored value's stored value is %x \n", **pp_a);

    printf("\n can we change a?, lets see \n");
    printf("\n p_a = p_b \n");
    p_a = p_b;
    printf("\n p_a's value is now: %x, same as 'p_b'... it seems we can, but can we do it in a function? lets see... \n", p_a);
    printf("\n cant_change(p_a, p_c); \n");
    cant_change(p_a, p_c);
    printf("\n p_a's value is now: %x, Doh! same as 'p_b'...  that function tricked us. \n", p_a);
    printf("\n a's value is now : %x \n", *p_a);
    printf("\n pp_a's value is %x \n", pp_a);
    printf("\n pp_a's stored value is %x \n", *pp_a);
    printf("\n pp_a's stored value's stored value is %x \n", **pp_a); 

    printf("\n NOW! lets see if a pointer to a pointer solution can help us... remember that 'pp_a' point to 'p_a' \n");
     printf("\n change(pp_a, p_c); \n");
    change(pp_a, p_c);
    printf("\n p_a's value is now: %x, YEAH! same as 'p_c'...  that function ROCKS!!!. \n", p_a);
    printf("\n a's value is now : %x \n", *p_a);
    printf("\n pp_a's value is %x \n", pp_a);
    printf("\n pp_a's stored value is %x \n", *pp_a);
    printf("\n pp_a's stored value's stored value is %x \n", **pp_a);
    printf("\n a(val) value is: %x", a);
    *p_a = 16;
    a = 76;
    p_a = &a;
    printf("\n a(val) value is: %x", a);
    printf("\n a's value: %x \n", *p_a);
    printf("\n p_a's value: %x \n", p_a); 
    return 0;
}

void cant_change(int * x, int * z){
    x = z;
    printf("\n ----> value of 'a' is: %x inside function, same as 'p_c', BUT will it be the same outside of this function? lets see\n", x);
}

void change(int ** x, int * z){
    *x = z;
    printf("\n ----> value of 'a' is: %x inside function, same as 'p_c', BUT will it be the same outside of this function? lets see\n", *x);
}
