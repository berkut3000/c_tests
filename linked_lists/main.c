#include <stdio.h>
#include <stdlib.h>

struct node {
    int value;
    struct node* next;
    struct node* prev;

};
typedef struct node node_t;

void printlist (node_t *head)
{
    node_t *temporary = head;
    while (temporary != NULL)
    {
        printf("%d - ", temporary->value);
        temporary = temporary->next;
    }
    printf("\n");
}

node_t *create_new_node(int value)
{
    node_t *result = malloc(sizeof(node_t));
    result->value = value;
    result->next = NULL;
    result->prev = NULL;
    return result;
}

node_t *insert_at_head(node_t **head, node_t *node_to_insert)
{
    node_to_insert->next = *head;
    if(*head != NULL)
    {
        (*head)->prev = node_to_insert;
    }
    *head = node_to_insert;
    node_to_insert->prev = NULL;
    return node_to_insert;

}

node_t *find_node(node_t *head, int value)
{
    node_t *tmp = head;
    while(tmp != NULL)
    {
        if(value == tmp->value )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}

node_t *find_last_node(node_t *head)
{
    node_t *tmp = head;
    while(tmp != NULL)
    {
        if(NULL == tmp->next )
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}


void insert_after_node(node_t *node_to_insert_after, node_t *newnode)
{
    newnode->next = node_to_insert_after->next;
    if(newnode->next != NULL)
    {
        newnode->next->prev = node_to_insert_after;
    }
    newnode->prev = node_to_insert_after;
    node_to_insert_after->next = newnode;

}

void remove_node(node_t **head, node_t * node_to_remove)
{
    if(*head == node_to_remove)
    {
        *head = node_to_remove->next;
        if (*head != NULL)
        {
            (*head)->prev = NULL;
        }
    }else{
        //find the previous node in the list
        node_to_remove->prev->next = node_to_remove->next;
        if(node_to_remove->next != NULL)
        {
            node_to_remove->next->prev = node_to_remove->prev;
        }
    }
    node_to_remove->next = NULL;
    node_to_remove->prev = NULL;
    return;

}

int main()
{
    node_t *head = NULL;
    node_t *tmp;

    insert_at_head(&head, create_new_node(45));
    insert_after_node(head, create_new_node(46));
    insert_after_node(head, create_new_node(46));
    insert_after_node(head, create_new_node(8));
    insert_after_node(head, create_new_node(56));
    insert_after_node(head, create_new_node(4));
    insert_after_node(head, create_new_node(58));

    tmp = find_last_node(head);
    printf("found node with value %d\n", tmp->value );



#if 0
// Main functionality
    for (int i = 0; i < 25; i++)
    {
        tmp = create_new_node(i);
        insert_at_head(&head, tmp);
    }

    tmp = find_node(head,9);
    printf("found node with value %d\n", tmp->value );

    insert_after_node(tmp, create_new_node(75));
    remove_node(&head,tmp);
    remove_node(&head,head);

#endif // 0


#if 0
    for (int i = 0; i < 25; i++)
    {
        tmp = create_new_node(i);
        tmp->next = head;
        head = tmp;
    }

#endif // 0

    #if 0
    tmp= create_new_node(32);
    head = tmp;
    tmp = create_new_node(8);
    tmp->next = head;
    head = tmp;
    tmp = create_new_node(45);
    tmp->next = head;
    head = tmp;
    #endif

    printlist(head);
    return 0;

#if 0


    node_t n1, n2, n3;
    node_t *head;
    n1.value = 45;
    n2. value = 8;
    n3.value = 32;

    // link them up.

    head= &n1;
    n3.next = NULL;
    n2.next = &n3;
    n1.next = &n2; // so we know when to stop.

    //printf("Hello world!\n");

#endif // 0
}
