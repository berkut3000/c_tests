#include <stdio.h>
#include <stdlib.h>
#include "csc.h"

int main()
{
    uint8_t array_1[100] = "Hola esta es una prueba de parseo";
    uint8_t array_2[10] = "Hola esta";
    uint8_t tam_string = strlen(array_1);
    uint8_t tam_substring = strlen(array_2);
    uint8_t status = custom_strstr(array_1, array_2, &tam_string, &tam_substring);

    printf("Hello world!\n");
    return 0;
}
