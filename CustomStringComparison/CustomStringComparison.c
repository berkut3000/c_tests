#include <stdio.h>
#include <stdint.h>

/* Definitions */
#define SUCCESS     0
#define UNSUCCESS   1
#define UN_EVAL     2

uint8_t my_strcmp(uint8_t *a, uint8_t *b);
uint8_t custom_strstr(uint8_t *a, uint8_t *b, uint8_t *expected_buffer_size, uint8_t *substring_size);

uint8_t main()
{
        uint8_t cadenita1[] = "HolaPinche";
        uint8_t cadenita2[] = "Hola";

        int8_t valor_resultante;
        valor_resultante = my_strcmp(cadenita1,cadenita2);
        printf("Cadena 1 mayor que cadena 2: %i\r\n",valor_resultante);

        valor_resultante = my_strcmp(cadenita2,cadenita1);
        printf("Cadena 1 menor que cadena 2: %i\r\n",valor_resultante);

        valor_resultante = my_strcmp(cadenita2,cadenita2);
        printf("Cadena 1 igual que Cadena 2 %i",valor_resultante);
	
	uint8_t cadenita3[] = "ok\r\n\0\0\0\0\0mac_tx_ok\r\n\0\0";
	uint8_t cadenita4[] = "esta es una cadena que no haya nada";
	uint8_t cadenita5[] = "mac_tx_ok";
	uint8_t resultado  = 0;
	uint8_t tamano_cadena = 0;
	uint8_t tamano_subcadena = 0;
	
	printf("Aquí se manda una subcadena mayor que la evaluada\r\n");
	resultado = custom_strstr(cadenita2, cadenita5, &tamano_cadena, &tamano_subcadena); 
	printf("Resultado = %i\r\n",resultado);

		
	return 0;
}

uint8_t my_strcmp(uint8_t *a, uint8_t *b)
{
        while (*a && *b && *a == *b)
        {
                ++a;
                ++b;
        }
        return (unsigned char)(*a) - (unsigned char)(*b);
}

/***************************************************************************//**
 * @brief
 *   Find string inside string.
 *
 * @details
 *   Compares 2 strings and finds 
 *
 * @note
 *   This was custom implemented, from an StackOVerflow function. Future
 *      revisions may or not include C's "strcmp() function.
 *
 * @param[in] uint8_t *a 
 *   Species starting postion of first string to be compared.
 *
 * @param[in] uint8_t *b
 *   Species starting postion of second string to be compared.
 *
 * @return
 *   The decimal representation of the difference in characters between the 2
 *   strings, expected value to be 0.
 ******************************************************************************/
uint8_t custom_strstr(uint8_t *a, uint8_t *b, uint8_t *expected_buffer_size, uint8_t *substring_size)
{
	
	uint8_t cont = 0;
	uint8_t cont_incidencias = 0;
	uint8_t criterio_search = *expected_buffer_size - *substring_size;
	uint8_t iter = 0;
	
	if(*expected_buffer_size < *substring_size)
	{
		return UN_EVAL;
	} 

	while(cont <= criterio_search)
	{       
                cont_incidencias = 0;
		for(iter = 0; iter < *substring_size; iter++)
		{
			if( *(a+iter+cont) == *(b+iter) )
			{
				cont_incidencias++;
			}
		}
		if(cont_incidencias == *substring_size)	
		{
			return SUCCESS;
		}
		cont++;
	}
    return UNSUCCESS;

}
