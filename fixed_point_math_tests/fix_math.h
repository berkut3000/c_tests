/*
 *  fix_math.h
 *
 *  Created on: 10/04/2019
 *      Author: AntoMota
 */

#ifndef __FIX_MATH_H__
#define __FIX_MATH_H___

/* Libraries to be included */
#include <stdint.h>

/* Definitions */
typedef union FIXED1_7tag {
    uint8_t full;
    struct part1_7tag {
    /* Little-Endian */ 
    uint8_t fraction: 7;
    uint8_t integer: 1;
    } part;
} FIXED1_7;

typedef union FIXED7_4tag {
    uint16_t full;
    struct part7_4tag {
    /* Little-Endian */
    uint16_t fraction: 9;
    uint16_t integer: 7;
    } part;
} FIXED7_4;


#define FIXED1_7CONST(A,B) (uint8_t)((A<<7) + ((B + 0.00390625)*128))
#define MULT1_7(A,B) (uint16_t)(A.full*B.full+64)>>7
#define DIV1_7(A,B)(uint16_t)((A.full<<8)/B.full)+1)/2

#define FIXED7_4CONST(A,B) (uint16_t)((A<<9) + ((B + 0.0009765625)*512))



#endif /* FILELOCATION_XXXX_H_ */
