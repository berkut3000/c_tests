#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "fix_math.h"

int main()
{
    FIXED1_7 temp,a,b;
    a.full = FIXED1_7CONST(1,0.8);
    b.full = FIXED1_7CONST(0,0.8);
    printf("Results of operations on 1_7 variables\n");
    temp.full = a.full + b.full;
    printf("Addition result is %d.%2.2d\n", temp.part.integer,
        (temp.part.fraction*100+64)/128);
    if (a.full < b.full)
    {
        printf("a is less than b. Subtraction overflows.\n");
    }
    if (a.full == b.full)
    {
        printf("a is the same as b. Result = 0.\n");
    }
    if (a.full > b.full)
    {
        temp.full = a.full - b.full;
        printf("Subtraction result is %d.%2.2d\n", temp.part.integer, 
        (temp.part.fraction*100+64)/128);
    }

    temp.full = MULT1_7(a,b);
    printf("Product of A and B is %d.%2.2d\n", temp.part.integer,
        (temp.part.fraction*100+64)/128);
    printf("\n Results of operations on 7_4 variables\n");
    FIXED7_4 tempx, c,d;
    c.full = FIXED7_4CONST(20, 0.1);
    printf("c = %x\n", c.full);
    printf("c integer value is: %x\n", c.part.integer);
    printf("c fractional value is: %1.1d\n", (c.part.fraction*10+256)/512);
    

    printf("\n Size of short: %d \n", sizeof(char));
    return 0;
}
