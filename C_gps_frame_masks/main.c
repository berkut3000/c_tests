#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define SUCCESS 0

typedef struct {
   float    sfUiLatitude;
   float    sfUiLongtitude;
   uint8_t  stringLatitude[20];
   uint8_t  stringLongitude[20];
} gps_struct;

uint8_t gps_frame_process(uint8_t * data_string, gps_struct geo_struct)
{
//    memset(&data_string[0],'\0',strlen(data_string));
//    uint8_t gps_cent_lat_flag = 0, gps_cent_long_flag = 0;
//    uint8_t gps_dec_lat_flag = 0, gps_dec_long_flag = 0;
//    uint8_t gps_uni_lat_flag = 0, gps_uni_long_flag = 0;
//    uint8_t gps_neg_lat_flag = 0, gps_neg_long_flag = 0;
//    uint8_t buff_lat_start = 0, buff_long_start = 0;
//
//    //if(gp)
//    if(0x2D == geo_struct.stringLatitude[0])
//    {
//        data_string[0] = (0x80 | data_string[0]);
//        gps_neg_lat_flag = 1;
//    }else{
//        data_string[0] = (0x00 | data_string[0]);
//        gps_neg_lat_flag = 0;
//    }
//
//    if(0x2D == geo_struct.stringLongitude[0])
//    {
//        data_string[0] = (0x40 | data_string[0]);
//        gps_neg_long_flag = 1;
//    }else{
//        data_string[0] = (0x00 | data_string[0]);
//        gps_neg_long_flag = 0;
//    }
//
//    if(geo_struct.sfUiLatitude > 99 | geo_struct.sfUiLatitude < -99 )
//    {
//        gps_cent_lat_flag = 1;
//    }else{
//        gps_cent_lat_flag = 0;
//    }
//
//    if(geo_struct.sfUiLongtitude > 99 | geo_struct.sfUiLongtitude < -99)
//    {
//        gps_cent_long_flag = 1;
//    }else{
//        gps_cent_long_flag = 0;
//    }
//
//    if(geo_struct.sfUiLatitude > 9 | geo_struct.sfUiLatitude < -9 )
//    {
//        gps_dec_lat_flag = 1;
//    }else{
//        gps_dec_lat_flag = 0;
//    }
//
//    if(geo_struct.sfUiLongtitude > 9 | geo_struct.sfUiLongtitude < -9)
//    {
//        gps_dec_long_flag = 1;
//    }else{
//        gps_dec_long_flag = 0;
//    }
//
//    if(geo_struct.sfUiLatitude > 0 | geo_struct.sfUiLatitude < 0 )
//    {
//        gps_uni_lat_flag = 1;
//    }else{
//        gps_uni_lat_flag = 0;
//    }
//
//    if(geo_struct.sfUiLongtitude > 0 | geo_struct.sfUiLongtitude < 0)
//    {
//        gps_uni_long_flag = 1;
//    }else{
//        gps_uni_long_flag = 0;
//    }


//
//    //buff_long_start = buff_long_start + gps_neg_long_flag;
//
//    /* Centesimal */
//        /* Latitude */
//    if(gps_neg_lat_flag == 1)
//    {
//        buff_lat_start += gps_neg_lat_flag;
//    }
//    if(gps_cent_lat_flag == 1)
//    {
//        data_string[1] |= ((geo_struct.stringLatitude[buff_lat_start] & 0x0F) << 4);
//    }else{
//        data_string[1] |= 0x00;
//    }
//
//    /* Longitude */
//    if(gps_neg_long_flag == 1)
//    {
//        buff_long_start += gps_neg_long_flag;
//    }
//    if(gps_cent_long_flag == 1)
//    {
//        data_string[1] |= (geo_struct.stringLongitude[buff_long_start] & 0x0F);
//    }else{
//        data_string[1] |= 0x00;
//    }
//
//    /* Decimal */
//    if(gps_cent_lat_flag == 1)
//    {
//        data_string[1+1] |= ((geo_struct.stringLatitude[buff_lat_start+1] & 0x0F) << 4);
//    }else{
//        data_string[1+1] |= 0x00;
//    }
//
//    /* Longitude */
//    if(gps_neg_long_flag == 1)
//    {
//        buff_long_start += gps_neg_long_flag;
//    }
//    if(gps_cent_long_flag == 1)
//    {
//        data_string[1+1] |= (geo_struct.stringLongitude[buff_long_start+1] & 0x0F);
//    }else{
//        data_string[1+1] |= 0x00;
//    }
//
//
//
//

    uint8_t units_lat = 0, units_long = 0;
    uint8_t dec_lat1 = 0, dec_lat2 = 0, dec_lat3 = 0, dec_lat4 =0;
    uint8_t dec_long1 = 0, dec_long2 = 0, dec_long3 = 0, dec_long4 =0;
    uint32_t place_holder_lat = 0, place_holder_long = 0;

//    Units
    /* Latitude */
    if(geo_struct.sfUiLatitude < 0)
    {
         units_lat = (uint8_t) (-1*geo_struct.sfUiLatitude);
         data_string[0] |= 0x80;
    }else{
         units_lat = (uint8_t) geo_struct.sfUiLatitude;
         data_string[0] |= 0x00;
    }
    data_string[1] = units_lat;

    /* Longiutde */
    if(geo_struct.sfUiLongtitude < 0)
    {
         units_long = (uint8_t) (-1*geo_struct.sfUiLongtitude);
         data_string[0] |= 0x40;
    }else{
         units_long = (uint8_t) geo_struct.sfUiLongtitude;
         data_string[0] |= 0x40;
    }
    data_string[5] = units_long;

//    Decimals
    if(geo_struct.sfUiLatitude < 0)
    {
          place_holder_lat = geo_struct.sfUiLatitude * -1000000;
    }else{
         place_holder_lat = geo_struct.sfUiLatitude * 1000000;
    }
    place_holder_lat -= units_lat * 1E6;

    /* Longiutde */
    if(geo_struct.sfUiLongtitude < 0)
    {
        place_holder_long = geo_struct.sfUiLongtitude * -1000000;
    }else{
         place_holder_long = geo_struct.sfUiLongtitude * 1000000;
    }
    place_holder_long -= units_long * 1E6;

    data_string[2] = (place_holder_lat & 0xFF0000)>>16;
    data_string[3] = (place_holder_lat & 0x00FF00)>>8;
    data_string[4] = (place_holder_lat & 0x0000FF);

    data_string[6] = (place_holder_long & 0xFF0000)>>16;
    data_string[7] = (place_holder_long & 0x00FF00)>>8;
    data_string[8] = (place_holder_long & 0x0000FF);

    return SUCCESS;
}

int main()
{
    uint8_t gps_frame[12] = "";
    memset(&gps_frame[0],'\0',strlen(gps_frame));
    gps_struct LatLongMyData;
    LatLongMyData.sfUiLatitude =    20.999999;
    LatLongMyData.sfUiLongtitude= -50.899997;
    memset(&LatLongMyData.stringLatitude[0],'\0',strlen(LatLongMyData.stringLatitude));
    memset(&LatLongMyData.stringLongitude[0],'\0',strlen(LatLongMyData.stringLongitude));
    strcat((char *)&LatLongMyData.stringLatitude[0], "20.1234560");
    strcat((char *)&LatLongMyData.stringLongitude[0], "-100.1234560");



    printf("Hello world!\n");
    gps_frame_process(gps_frame, LatLongMyData);
    for (int8_t i = 0; i < strlen(gps_frame); i++)
    {
        printf("%#02x\n", gps_frame[i]);   // gives 0x000007
    }

    return SUCCESS;
}
