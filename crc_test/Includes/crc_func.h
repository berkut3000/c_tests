#ifndef CRC_FUNC_H_INCLUDED
#define CRC_FUNC_H_INCLUDED

extern const unsigned short T_U16_CRC_TABLE[256];
unsigned short FN_CalcCRC(unsigned char * lpu8Buf, unsigned short lu16Len);

#endif // CRC_FUNC_H_INCLUDED
