#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "Includes/crc_func.h"

int main()
{
    uint16_t u16crc         = 0xF0F0;
    uint16_t u16test_var    = 0x0000;
    unsigned char cad_buff[20] = "123456789";



    u16test_var = u16crc ^ 0xFFFF;
    u16test_var = FN_CalcCRC(cad_buff,9);
    printf("%04X", u16test_var);
    return 0;
}
