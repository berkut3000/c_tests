/*******************************************************************************
 * toascii.h
 *
 *  Created on: 13/06/2019
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __TOASCII_H__
#define __TOASCII_H__

/* Libraries to be included */
#include <stdint.h>

/* Function prototypes of the source file this header file references to */
uint8_t hex_to_ascii(uint8_t * data_string);
uint8_t int_to_ascii(uint8_t * data_string);

#endif /* __TOASCII_H__ */
/*** end of file ***/
