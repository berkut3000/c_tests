/******************************************************************************
 * @ file toascii.c
 *
 *  Created on: 13/06/2018
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <toascii.h>

/* New data types */

/* Constant defintions */

/* Macro definitions */
#define SUCCESS     0

/* Static Data */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *  Converts Hex string to ascii.
 *
 * @details
 *   Converts Hex string to ascii string.
 *
 * @note
 *   For use for serial comm to RN2903, for example. All the data must be a
 *      printable ASCII hex string
 *
 * @param[in] data_string
 *   Pointer to string array containing the data to be converted..
 *
 * @return
 *   Zero if SUCCESS.
 ******************************************************************************/
uint8_t  hex_to_ascii(uint8_t * data_string)
{
    return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *  Converts int to ascii.
 *
 * @details
 *   Converts int numbers to ascii string.
 *
 * @note
 *   For use for serial comm to RN2903, for example. All the array contents must
 *      valid number
 *
 * @param[in] number
 *   Pointer to number containing the data to be converted..
 *
 * @return
 *   Zero if SUCCESS.
 ******************************************************************************/
uint8_t int_to_ascii(uint8_t * number)
{
    return SUCCESS;
}

/* Private Functions */

/*** end of file ***/