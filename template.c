/******************************************************************************
 * @ file template.c
 *
 *  Created on: DD/MM/2019
 *  Modified on: DD/MM/2019
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <XXXXX.h>

/* New data types */

/* Constant defintions */

/* Macro definitions */

/* Static Data */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields.
 ******************************************************************************/
void foo()
{

}


/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields
 ******************************************************************************/
int16_t bar(*param 1, *param 2)
{

}

/* Private Functions */

/*** end of file ***/