#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "crc_func.h"

int main()
{
    uint16_t u16crc         = 0x0000;
    uint16_t u16test_var    = 0x0000;
    /* uint8_t cad_buff[100] = "Cadena LOCA de los perros del mal Ivan ponte a trabajar"; */
    /* uint8_t cad_buff[9] = "123456789"; */
    uint8_t cad_buff[15] = {0x5A,0x08,0x01,0x02,0x05,0x09,0x53,0x54,0x4F,0x50,0x20,0x00,0x00,0x3D,0x1A};


    printf("Programa de prueba de CRC-16/XModem\r\n");
    /* u16test_var = u16crc ^ 0xFFFF; */
    u16test_var = ((uint16_t) cad_buff[13]<<8 | (uint16_t) cad_buff[14]);
    u16crc = FN_CalcCRC(cad_buff,13);
    printf("CRC obtenido\t = %04X\r\n", u16test_var);
    printf("CRC calculadodo\t = %04X\r\n", u16crc);
    if(u16test_var == u16crc)
    {
        printf("%04X\r\n", u16crc);
    }
    
    return 0;
}
