/*
 * crc_func.h
 *
 *  Created on: 29/10/2018
 *      Author: AntoMota
 */

#ifndef __CRC_FUNC_H_INCLUDED__
#define __CRC_FUNC_H_INCLUDED__

/* Libraries to be included */
#include <stdint.h>

/* Definitions */

/* Structs */

extern const uint16_t T_U16_CRC_TABLE[256];
uint16_t FN_CalcCRC(uint8_t * lpu8Buf, uint16_t lu16Len);

#endif // __CRC_FUNC_H_INCLUDED__
